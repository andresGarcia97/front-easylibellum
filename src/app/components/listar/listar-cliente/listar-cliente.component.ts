import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-cliente',
  templateUrl: './listar-cliente.component.html',
  styleUrls: ['./listar-cliente.component.css']
})
export class ListarClienteComponent implements OnInit {
  clientes: Cliente[] = [];
  clienteSeleccionado = new Cliente();
  constructor(private clienteService: ClienteService, private router: Router) { }

  ngOnInit() {
    this.clienteService.listAll().subscribe(
      (clientes) => {
        this.clientes = clientes;
      }
    );
  }

  showPopup(cliente: Cliente) {
    this.clienteSeleccionado = cliente;
  }

  delete(clienteSeleccionado: Cliente) {
    this.clienteService.delete(clienteSeleccionado.identificacion).subscribe(
      _ => {
        this.clientes = this.clientes.filter(cliente => cliente !== clienteSeleccionado);
      }
    );
  }

  update() {
    this.clienteService.update(this.clienteSeleccionado).subscribe(
      _ => this.router.navigate(['/listarclientes'])
    );
  }

}
