import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ListarClienteComponent } from './components/listar/listar-cliente/listar-cliente.component';
import { ListarEmpleadoComponent } from './components/listar/listar-empleado/listar-empleado.component';
import { ListarFacturaComponent } from './components/listar/listar-factura/listar-factura.component';
import { ListarProductoComponent } from './components/listar/listar-producto/listar-producto.component';
import { MenuComponent } from './components/menu/menu.component';
import { RegistroClienteComponent } from './components/registro/registro-cliente/registro-cliente.component';
import { RegistroEmpleadoComponent } from './components/registro/registro-empleado/registro-empleado.component';
import { RegistroFacturaComponent } from './components/registro/registro-factura/registro-factura.component';
import { RegistroProductoComponent } from './components/registro/registro-producto/registro-producto.component';
import { ClienteService } from './services/cliente.service';
import { EmpleadoService } from './services/empleado.service';
import { FacturaService } from './services/factura.service';
import { ProductoService } from './services/producto.service';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ListarClienteComponent,
    ListarEmpleadoComponent,
    ListarFacturaComponent,
    ListarProductoComponent,
    MenuComponent,
    RegistroClienteComponent,
    RegistroEmpleadoComponent,
    RegistroFacturaComponent,
    RegistroProductoComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ClienteService,
    EmpleadoService,
    FacturaService,
    ProductoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
