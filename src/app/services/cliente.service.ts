import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Cliente } from '../models/cliente';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private ENDPOINTBASE: 'http://localhost:8080/clientes';
  private headersText = new HttpHeaders({'Content-Type': 'text/plain'});
  private headersjson = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {}

    listAll(): Observable <Cliente [] > {
      return this.http.get< Cliente [] >(this.ENDPOINTBASE.concat('/listar'));
    }

    find(id: string): Observable <Cliente> {
      return this.http.get<Cliente>(this.ENDPOINTBASE.concat('/cliente/').concat(id), {headers: this.headersText});
    }

    delete(id: string): Observable <Cliente> {

      return this.http.delete <Cliente> (this.ENDPOINTBASE.concat('/eliminar/').concat(id), {headers: this.headersText});
    }

    create(cliente: Cliente): Observable<Cliente> {
      return this.http.post <Cliente> (this.ENDPOINTBASE.concat('/insertar'), cliente, {headers: this.headersjson});
    }

    update(cliente: Cliente): Observable<Cliente> {
      return this.http.put <Cliente> (this.ENDPOINTBASE.concat('/actualizar'), cliente, {headers: this.headersjson});
    }
}
