import { DetalleFactura } from './detalle-factura';
import { Cliente } from './cliente';
import { Empleado } from './empleado';

export class Factura {

public idFactura: number;
public fechaVenta: Date;
public productosYCantidades: DetalleFactura [];
public valorTotal: number;
public cliente: Cliente;
public empleado: Empleado;
}
