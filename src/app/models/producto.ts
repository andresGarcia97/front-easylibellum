export class Producto {

    public codigoBarras: number;
    public nombre: string;
    public marca: string;
    public iva: number;
    public precioCompra: number;
    public precioVenta: number;
}
