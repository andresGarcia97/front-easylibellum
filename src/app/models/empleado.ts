export class Empleado {

    identificacion: string;
    usuario: string;
    contrasena: string;
    nombre: string;
    apellido: string;
    rol: string;
    telefono: string;
    sueldo: number;
    fechaNacimiento: Date;
}
