import { Producto } from 'src/app/models/producto';

export class DetalleFactura {

    public idFactura: number;
    public cantidadProducto: number;
    public producto: Producto;
}
