import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { ListarProductoComponent } from './components/listar/listar-producto/listar-producto.component';
import { ListarClienteComponent } from './components/listar/listar-cliente/listar-cliente.component';
import { ListarEmpleadoComponent } from './components/listar/listar-empleado/listar-empleado.component';

const routes: Routes = [
  {path : 'listarproductos', component: ListarProductoComponent},
  {path : 'listarclientes', component: ListarClienteComponent},
  {path : 'listarempleados', component: ListarEmpleadoComponent},
  {path : 'inicio', component: InicioComponent},
  {path : '**', pathMatch: 'full', redirectTo: 'inicio'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
